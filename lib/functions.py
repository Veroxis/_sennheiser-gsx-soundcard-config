import os, subprocess

supported_devices = ["1000", "1200"]

def exit_with_error_message(msg):
	log(f"Error: {msg}")
	exit(1)

def check_elevated_permission():
	if (os.geteuid() == 0):
		exit_with_error_message("Make sure to run this script as regular user in the sudoers file.")

def log(msg):
	print(f"-> {msg}")

def copy_file_sudo(src, target):
	if os.path.exists(src):
		log(f"  Copy File: {src} -> {target}")
		subprocess.call(['sudo', 'cp', src, target])

def delete_file_sudo(target):
	if os.path.exists(target):
		log(f"  Delete File: {target}")
		subprocess.call(['sudo', 'rm', target])

def exec_sys_command(cmd):
	log(f"  System Command: {cmd}")
	subprocess.call(cmd)

def copy_config_files():
	script_path = os.path.dirname(__file__)
	log("Copying config files...")
	copy_file_sudo(f"{script_path}/../config_files/usr/share/pulseaudio/alsa-mixer/profile-sets/sennheiser-gsx.conf", "/usr/share/pulseaudio/alsa-mixer/profile-sets/sennheiser-gsx.conf")
	for device in supported_devices:
		log(f"Device [{device}]")
		copy_file_sudo(f"{script_path}/../config_files/lib/udev/rules.d/91-pulseaudio-gsx-{device}.rules", f"/lib/udev/rules.d/91-pulseaudio-gsx-{device}.rules")
		copy_file_sudo(f"{script_path}/../config_files/etc/X11/xorg.conf.d/40-sennheiser-gsx-{device}.conf", f"/etc/X11/xorg.conf.d/40-sennheiser-gsx-{device}.conf")
	log("Config files are ready")

def delete_config_files():
	log("Deleting config files...")
	delete_file_sudo("/usr/share/pulseaudio/alsa-mixer/profile-sets/sennheiser-gsx.conf")
	for device in supported_devices:
		log(f"Device [{device}]")
		delete_file_sudo(f"/lib/udev/rules.d/91-pulseaudio-gsx-{device}.rules")
		delete_file_sudo(f"/etc/X11/xorg.conf.d/40-sennheiser-gsx-{device}.conf")
	log("Config files have been deleted")

def reload_services():
	log("Reloading services...")
	exec_sys_command(['sudo', 'udevadm', 'control', '-R'])
	exec_sys_command(['sudo', 'udevadm', 'trigger'])
	exec_sys_command(['pulseaudio', '-k'])
	log("Services have been reloaded")
