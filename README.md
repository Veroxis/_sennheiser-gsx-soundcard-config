# Sennheiser GSX 1000 & 1200 pulseaudio setup

This repository contains configuration files for enabling 7.1 sound support through pulseaudio using the sennheiser gsx 1000 or 1200 soundcards.

## Installation (manually)

Copy the configuration files within the 'config_files' directory into their corresponding system path. Then reload the sound server either by rebooting or using these commands:

```sh
sudo udevadm control-R
sudo udevadm trigger
pulseaudio -k
```

## Installation (automated)

Execute the installer through a regular user within the sudoers file:

```sh
./install.py
```

The script was written using python 3.8. Results in different versions may vary. You can always fall back to the manual install.

## Tested on

- Arch Linux

## Credits

Thanks to [this post](https://smartshitter.com/musings/2018/12/sennheiser-gsx-1000-under-linux-take-two/) which i've derived the configuration from.
