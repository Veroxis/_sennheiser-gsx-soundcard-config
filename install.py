#!/usr/bin/env python

from lib import functions

functions.check_elevated_permission()
functions.copy_config_files()
functions.reload_services()
